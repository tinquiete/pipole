#!/usr/bin/python3
import numpy as np
from utils.tabulate import tabulate as tabt
# from tabulate import tabulate as tabt

import search_methods as srch
from demo_script.load_data import load
from demo_script.segmentation import metriques, seg, bkps_cost
from show import display


RES_FILE = "res_table.txt"


def demo(fname, algo_list):
    signal, bkps = load(fname)
    # table of results
    header = ["", "Sum of costs", "True sum of costs", "RandIndex",
              "Hausdorff", "Precision", "Recall"]
    tab = list()

    # true breakpoints
    fig, _ = display(signal, bkps)
    fig.savefig("true.png")

    for k, param in enumerate(algo_list, start=1):
        algo_str, model_str = param.split("_")
        res_fname = "res_{}.png".format(k)

        true_cost_val = bkps_cost(signal, bkps, model_str)
        # selected segmentation
        if algo_str != "Omp":
            algo = getattr(srch, algo_str)(model=model_str)
        else:
            algo = getattr(srch, algo_str)()

        my_bkps = seg(signal, algo, bkps)
        cost = bkps_cost(signal, my_bkps, model_str)

        row_title = "{} ({})".format(
            algo_str, model_str.replace("constant", ""))
        row = [row_title] + [cost, true_cost_val] + metriques(bkps, my_bkps)
        tab.append(row)
        fig, axarr = display(signal, bkps, my_bkps)
        axarr[0].set_title(row_title, fontsize=40)
        # fig.suptitle(row_title, fontsize=40)

        fig.savefig(res_fname, bbox_inches='tight')

    # write table
    with open(RES_FILE, "w") as res_file:
        table = tabt(tab, headers=header, floatfmt=".2f")
        print(table, file=res_file)
        res_file.close()
