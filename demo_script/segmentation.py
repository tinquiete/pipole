"""Compute the segmentation"""
from metrics import randindex, hausdorff, precision_recall
from utils import pairwise
from search_methods import Dynp


def seg(signal, algo, bkps):
    """Compute breakpoints and associated cost."""
    n_samples = bkps[-1]
    n_bkps = len(bkps) - 1

    my_bkps = algo.fit_predict(signal=signal, n_bkps=n_bkps)
    return my_bkps


def bkps_cost(signal, bkps, model_str):
    """Compute the sum of cost for the provided breakpoints."""
    dyn = Dynp(model=model_str)
    n_samples = bkps[-1]
    dyn.fit(signal)
    mean_cost = sum(dyn.cost.error(start, end)
                    for start, end in pairwise([0] + bkps))
    mean_cost /= n_samples
    return mean_cost


def metriques(bkps, my_bkps):
    """randindex, hausdorff, precision, recall."""
    n_samples = bkps[-1]
    ran = randindex(bkps, my_bkps)
    hau = hausdorff(bkps, my_bkps) / n_samples
    pre, rec = precision_recall(bkps, my_bkps, margin=int(0.05 * n_samples))
    return [ran, hau, pre, rec]
