import numpy as np

from utils import pairwise


def load(fname):
    """Returns the signal and breakpoint indexes."""
    filecontent = np.loadtxt(fname)
    signal = filecontent[:, :-1]
    states = filecontent[:, -1]

    n_samples = signal.shape[0]
    bkps = [ind for (ind, (x, y)) in enumerate(
        pairwise(states), start=1) if x != y]
    bkps += [n_samples]

    return signal, bkps
