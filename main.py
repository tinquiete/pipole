#!/usr/bin/python3
"""Command line interface."""
import argparse

import matplotlib
matplotlib.use("Agg")

from demo_script.demo import demo


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Change point detection.')
    parser.add_argument("--input", "-i",
                        help='Input file containing the signal.',
                        required=True)
    parser.add_argument("--algo", "-a",
                        choices=[
                            'Binseg_constantl1', 'Binseg_constantl2', 'Binseg_rbf',
                            'BottomUp_constantl1', 'BottomUp_constantl2',
                            'BottomUp_rbf', 'Omp_constantl2',
                            'Dynp_constantl1', 'Dynp_constantl2', 'Dynp_rbf'],
                        help='Algorithm used to perform the change point detection.',
                        required=True,
                        nargs="+")

    args = parser.parse_args()
    demo(args.input, args.algo)
