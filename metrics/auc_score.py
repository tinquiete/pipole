import numpy as np

from utils import unzip


def auc(x, y):
    """Area under the curve using the composite trapezoidal rule.

    Assume that the x vary inside [0, 1].
    The curve is extended to fit inside [0, 1].

    Args:
        x (array): shape (n,)
        y (array): shape (n,)

    Returns:
        float: auc
    """
    points = list(zip(x, y))
    points.sort(key=lambda t: (t[0], -t[1]))
    # add extremities
    x_0, y_0 = points[0]
    if x_0 > 0:
        points.insert(0, (0, y_0))
    x_0, y_0 = points[-1]
    if x_0 < 1:
        points.append((1, y_0))
    x_x, y_y = unzip(points)
    area = np.trapz(y_y, x=x_x)
    return area
