class NotEnoughPoints(Exception):
    """Raise this exception when there is not enough point to calculate a
    cost function """
    pass
